defmodule Rentit.Repo.Migrations.CreateSalesBookings do
  use Ecto.Migration

  def change do
    create table(:sales_bookings) do
      add :name, :string
      add :start_date, :date
      add :end_date, :date

      timestamps()
    end

  end
end
