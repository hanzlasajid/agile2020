defmodule Rentit.Repo.Migrations.CreateSalesCars do
  use Ecto.Migration

  def change do
    create table(:sales_cars) do
      add :name, :string
      add :description, :string
      add :price, :string

      timestamps()
    end

  end
end
