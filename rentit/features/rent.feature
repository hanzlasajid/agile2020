Feature: Car renting
  As a customer
  Such that I go to destination
  I want to rent a car

  Scenario: Rent via RENTIT' web page (with confirmation)
    Given the following cars are available
        | name     | description  | price  |
        | toyota   | jeep         | 120	   |
        | honda    | mini         | 100    |
    And I want to book car name "toyota" from "2018-09-01" to "2018-09-05"
     And I open RENTIT' web page
    And I enter the car name
    And I enter the booking information 
    When I submit the booking request
    Then I should receive a confirmation message