defmodule WhiteBreadContext do
  use WhiteBread.Context
  use Hound.Helpers

  alias Rentit.Sales.Booking
  alias Rentit.Repo

  feature_starting_state fn  ->
    Application.ensure_all_started(:hound)
    %{}
  end
  scenario_starting_state fn _state ->
    Hound.start_session
    #Ecto.Adapters.SQL.Sandbox.checkout(Rentit.Repo)
    #Ecto.Adapters.SQL.Sandbox.mode(Rentit.Repo, {:shared, self()})
    %{}
  end
  scenario_finalize fn _status, _state ->
    #Ecto.Adapters.SQL.Sandbox.checkin(Rentit.Repo)
    #Hound.end_session
  end

  given_ ~r/^the following cars are available$/,
  fn state, %{table_data: table} ->
    table
    |> Enum.map(fn car_data -> Booking.changeset(%Booking{}, car_data) end)
    |> Enum.each(fn changeset -> Repo.insert!(changeset) end)
    {:ok, state}
  end

  and_ ~r/^I want to book car name "(?<name>[^"]+)" from "(?<start_date>[^"]+)" to "(?<end_date>[^"]+)"$/,
  fn state, %{name: name,start_date: start_date,end_date: end_date} ->
    {:ok, state |> Map.put(:name, name) |> Map.put(:start_date, start_date) |> Map.put(:end_date, end_date)}
  end

  and_ ~r/^I open RENTIT' web page$/, fn state ->
    navigate_to "/bookings"
    {:ok, state}
  end

  and_ ~r/^I enter the car name$/, fn state ->
    {:ok, state}
  end

  and_ ~r/^I enter the booking information $/, fn state ->
    fill_field({ :id, "name"}, state[:name])
    fill_field({ :id, "start_date"}, state[:start_date])
    fill_field({ :id, "end_date"}, state[:end_date])
    {:ok, state}
  end

  when_ ~r/^I submit the booking request$/, fn state ->
    click({:id, "submit_button"})
    {:ok, state}
  end

  then_ ~r/^I should receive a confirmation message$/, fn state ->
    {:ok, state}
  end

end
