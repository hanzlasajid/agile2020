defmodule RentitWeb.BookingController do
  use RentitWeb, :controller
  import Ecto.Query, only: [from: 2]
  alias Rentit.Sales.Booking
  alias Rentit.Repo

  def index(conn, _params) do
    render conn, "index.html", changeset: Booking.changeset(%Booking{}, %{})
  end

  def create(conn,%{"booking" => booking_params}) do
    query = from t in Booking, where: t.name == ^booking_params["name"], select: t
    available_cars = Repo.all(query)

    if(length(available_cars) > 0) do
      changeset = Booking.changeset(%Booking{}, booking_params)
      Repo.insert(changeset)
      conn
      |> put_flash(:info, "your booking has been confirmed")
      |> redirect(to: page_path(conn, :index))
    else
      conn
      |> put_flash(:error, "Our apologies, no car is available at this moment. Please wait")
      |> redirect(to: page_path(conn, :index))
    end
  end

end
