defmodule Rentit.Sales.Booking do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_bookings" do
    field :end_date, :date
    field :name, :string
    field :start_date, :date

    timestamps()
  end

  @doc false
  def changeset(booking, attrs) do
    booking
    |> cast(attrs, [:name, :start_date, :end_date])

  end
end
