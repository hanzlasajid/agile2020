defmodule Rentit.Sales.Car do
  use Ecto.Schema
  import Ecto.Changeset


  schema "sales_cars" do
    field :description, :string
    field :name, :string
    field :price, :string

    timestamps()
  end

  @doc false
  def changeset(car, attrs) do
    car
    |> cast(attrs, [:name, :description, :price])
  end
end
